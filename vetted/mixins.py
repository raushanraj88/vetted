from django.contrib.auth.mixins import AccessMixin

class OverallAdminMixin(AccessMixin):
    """
    return: Verify that the current user is authenticated and is overalladmin
    """
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated or request.user.user_role!="1":
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)


class CompanyAdminMixin(AccessMixin):
    """
    return: Verify that the current user is authenticated.
    """
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated or request.user.user_role!="2":
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)

class RequestFormKwargsMixin(object):
    """
    return: Mixin will help to set request object to be used in form methods.
    """
    def get_form_kwargs(self):
        kwargs = super(RequestFormKwargsMixin,self).get_form_kwargs()
        kwargs.update({"request":self.request})
        return kwargs