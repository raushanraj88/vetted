from django.db import models


class DateTimeStampedModel(models.Model):
    """
    return: Abstract model for all other model, it will keep created and updated date time in table structure
    """
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True