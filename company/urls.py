# -*- coding: utf-8 -*-
from django.urls import path, re_path
from company import views

app_name = "company"
urlpatterns = [

    re_path(
       r'^list/$',
       views.CompanyListView.as_view(),
       name='list'
    ),

    # URL pattern for the UserUpdateView
    re_path(
        r'^add/$',
        views.CompanyCreateView.as_view(),
        name='add'
    ),

    re_path(
        r'^delete/(?P<pk>\d+)/$',
        views.CompanyDeleteView.as_view(),
        name='delete'
    ),


]