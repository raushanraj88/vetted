from django.db import models

from users.models import  User
from vetted.models import DateTimeStampedModel
# Create your models here.

class Company(DateTimeStampedModel):
    name = models.CharField(max_length=255,blank=True)
    email_domain = models.CharField(max_length=255,unique=True)
    admin = models.EmailField(max_length=255,unique=True)
    created_by = models.ForeignKey("users.User",on_delete=models.CASCADE,related_name="created_by")
    created_for = models.ForeignKey("users.User",on_delete=models.CASCADE,related_name="created_for")

    def save(self,*args,**kwargs):
        u=User.objects.filter(email=self.admin)
        u.update(role="2")
        self.created_for=u.first()
        super(Company,self).save(*args,**kwargs)
