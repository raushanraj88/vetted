import validators

from django import forms

from .models import Company
from users.models import User

class CompanyForm(forms.ModelForm):
    """
    return: The company creation form used by overalladmin to create new company
    """

    def clean_domain(self):
        """
        :return: The email_domain is to restrict user assignment within the organisation's email namespace. In future
        company will be active only when admin verify it's email_domain
        """
        domain=self.cleaned_data['email_domain']
        flag = validators.domain(domain)
        if flag == True:
            return domain
        else:
            raise forms.ValidationError("Domain must be like google.com,sites.google.com, etc")

    def clean_admin(self):
        """
        :return: Only signed up user can be assigned an admin role for any company
        """
        email = self.cleaned_data["admin"]
        try:
            User.objects.get(email=email)
        except:
            raise forms.ValidationError("User with this email never signed up")
        return email


    class Meta:
        model = Company
        fields = ("name","email_domain","admin")