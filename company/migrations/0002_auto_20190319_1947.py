# Generated by Django 2.1.7 on 2019-03-19 19:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='admin',
            field=models.EmailField(max_length=255),
        ),
    ]
