from django.shortcuts import render
from django.views.generic import ListView, CreateView, DeleteView
from django.urls import reverse
from django.http import Http404

from .models import Company
from .forms import CompanyForm

from django.contrib.auth.mixins import LoginRequiredMixin
from vetted.mixins import OverallAdminMixin


class CompanyListView(OverallAdminMixin,ListView):
    """
    return: List of all the companies created by overall admin
    """
    model = Company
    paginate_by = 10


class CompanyCreateView(OverallAdminMixin, CreateView):
    """
    return: The view will create a company and will assign admin to it
    """
    model = Company
    form_class = CompanyForm

    def get_success_url(self):
        return reverse('company:list')

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(CompanyCreateView, self).form_valid(form)

class CompanyDeleteView(OverallAdminMixin, DeleteView):
    """
    return: The view will delete company requested by overalladmin
    """
    def get_object(self,*args,**kwargs):
        pk = self.kwargs["pk"]
        try:
            object = Company.objects.get(pk=pk)
        except:
            raise Http404
        return object

    def get_success_url(self):
        return reverse('company:list')