from django.contrib.auth.models import AbstractUser

# Import the basic Django ORM models library
from django.db import models

# Subclass AbstractUser
class User(AbstractUser):
    ROLES = (("1","OVERALL_ADMIN"),("2","COMPANY_ADMIN"),("3","EMPLOYEE"))
    role = models.CharField(choices=ROLES,max_length=2,blank=True)
    def __unicode__(self):
        return self.username

    @property
    def user_role(self):
        return self.role

    @property
    def company(self):
        return self.created_for.first()