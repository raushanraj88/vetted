# -*- coding: utf-8 -*-
from django.urls import path, re_path
from employee import views

app_name = "employee"
urlpatterns = [

    re_path(
       r'^list/$',
       views.EmployeeListView.as_view(),
       name='list'
    ),

    # URL pattern for the UserUpdateView
    re_path(
        r'^add/$',
        views.EmployeeCreateView.as_view(),
        name='add'
    ),

    re_path(
        r'^delete/(?P<pk>\d+)/$',
        views.EmployeeDeleteView.as_view(),
        name='delete'
    ),

   re_path(
        r'^~update/$',
        views.EmployeeProfileUpdateView.as_view(),
        name='profile_update'
    ),
    re_path(
        r'^me/$',
        views.EmployeeProfileDetailView.as_view(),
        name='profile_detail'
    ),
    re_path(
        r'^invite/$',
        views.InvitationCreateView.as_view(),
        name='invite'
    ),

]