from django import forms

from .models import Employee, Invitation
from users.models import User

class EmployeeForm(forms.ModelForm):
    email = forms.EmailField(required=True)

    def __init__(self,*args,**kwargs):
        self.request = kwargs.pop("request")
        super(EmployeeForm, self).__init__(*args,**kwargs)

    def clean_email(self):
        """
        :return: Raise validation error if created employee email domain does'nt match with it's creator company email domain.
        """
        email = self.cleaned_data["email"]
        #check if email already associated
        try:
            u=User.objects.get(email=email)
        except:
            raise forms.ValidationError("The user does'nt exist with this email, please invite him")

        if self.request.user.company.email_domain != email.split("@")[1]:
            raise forms.ValidationError("Company domain must match with email domain i.e. "+self.request.user.company.email_domain)
        return email

    class Meta:
        model = Employee
        fields = ("name","email")

    # def save(self):
    #     email=self.cleaned_data['email']
    #     self.user = User.objects.get(email=email)
    #     return super(EmployeeForm, self).save()


class EmployeeProfileForm(forms.ModelForm):
    """
    return: This form is for employee profile edit only.
    """
    class Meta:
        model =  Employee
        fields = ("name","about_me")

class InvitationForm(forms.ModelForm):

    def __init__(self,*args,**kwargs):
        self.request = kwargs.pop("request")
        super(InvitationForm, self).__init__(*args,**kwargs)

    def clean_email(self):
        """
        :return: Invitaion created only when it's email domain match with creator's company email domain
        """
        email = self.cleaned_data["email"]
        user=User.objects.filter(email=email)
        if user:
            raise forms.ValidationError("User with this email already exist.")
        if self.request.user.company.email_domain != email.split("@")[1]:
            raise forms.ValidationError("Company domain must match with email domain i.e. " + self.request.user.company.email_domain)
        return email

    class Meta:
        model = Invitation
        fields = ("email",)

