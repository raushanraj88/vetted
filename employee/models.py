from django.db import models
from django.core import mail
from django.dispatch import receiver

from allauth.account.signals import user_signed_up

from vetted.models import  DateTimeStampedModel


class Employee(DateTimeStampedModel):
    name = models.CharField(max_length=255,blank=True)
    about_me = models.TextField(blank=True)
    user = models.OneToOneField("users.User",null=True, on_delete=models.CASCADE) #models.SET_NULL not required
    company = models.ForeignKey("company.Company",null=True,on_delete= models.CASCADE)
    created_by = models.ForeignKey("users.User",null=True,on_delete= models.CASCADE,related_name="company_admin")

    def save(self,*args,**kwargs):
        """
        :param args:
        :param kwargs:
        :return:  update user role with that of employee in first creation
        """
        if not self.pk:
            self.user.role = "3"
            self.user.save()
        return super(Employee,self).save(*args,**kwargs)


class Invitation(DateTimeStampedModel):
    company = models.ForeignKey("company.Company", null=True, on_delete=models.CASCADE)
    sent_by = models.ForeignKey("users.User",on_delete=models.CASCADE,related_name="invited_by")
    email = models.EmailField(unique=True)

    def save(self,*args,**kwargs):
        send_mail=mail.send_mail("You are invited to join vetted","","Vetted invitation <noreply@vetted.com>",[self.email],html_message='')
        super(Invitation,self).save(*args,**kwargs)

@receiver(user_signed_up)
def user_signed_up_(request,user,**kwargs):
    """
    :param request:
    :param user:
    :param kwargs:
    :return:
    Delete the invitation object if sent any and assign employee and company to user
    """
    try:
        inv_obj = Invitation.objects.get(email=user.email)
        Employee.objects.create(user=user,company= inv_obj.company, created_by=inv_obj.sent_by)
        user.role = "3"
        user.save()
        inv_obj.delete()
    except Exception as e:
        print(e)