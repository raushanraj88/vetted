from django.shortcuts import render
from django.views.generic import ListView, CreateView, DeleteView, UpdateView, DetailView
from django.urls import reverse
from django.http import Http404
from django.contrib import messages

from .models import Employee, Invitation
from .forms import EmployeeForm, EmployeeProfileForm, InvitationForm
from users.models import User

from django.contrib.auth.mixins import LoginRequiredMixin
from vetted.mixins import CompanyAdminMixin, RequestFormKwargsMixin


class EmployeeListView(CompanyAdminMixin,ListView):
    model = Employee
    paginate_by = 10

    def get_queryset(self):
        return Employee.objects.filter(company = self.request.user.company)

class EmployeeCreateView(CompanyAdminMixin, RequestFormKwargsMixin, CreateView):
    model = Employee
    form_class = EmployeeForm

    def get_success_url(self):
        return reverse('employee:list')

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        form.instance.company = self.request.user.company
        form.instance.user  = User.objects.get(email=form.data["email"])
        return super(EmployeeCreateView, self).form_valid(form)


class EmployeeDeleteView(CompanyAdminMixin, DeleteView):

    def get_object(self,*args,**kwargs):
        pk = self.kwargs["pk"]
        try:
            object = Employee.objects.get(pk=pk,company=self.request.user.company)
        except:
            raise Http404
        return object

    def get_success_url(self):
        return reverse('employee:list')

class EmployeeProfileUpdateView(LoginRequiredMixin,UpdateView):
    form_class = EmployeeProfileForm
    model = Employee
    template_name ="employee/employee_profile_form.html"

    def get_success_url(self):
        return reverse("home")

    def get_object(self, *args, **kwargs):
        return Employee.objects.get(user=self.request.user)

class EmployeeProfileDetailView(LoginRequiredMixin,DetailView):
    model = Employee
    template_name ="employee/employee_profile_detail.html"

    def get_success_url(self):
        return reverse("employee:detail")

    def get_object(self, *args, **kwargs):
        return Employee.objects.get(user=self.request.user)

class InvitationCreateView(CompanyAdminMixin, RequestFormKwargsMixin, CreateView):
    model = Invitation
    form_class = InvitationForm

    def get_success_url(self):
        messages.success(self.request,"Invitation mail sent on console")
        return reverse('employee:invite')

    def form_valid(self, form):
        form.instance.sent_by = self.request.user
        form.instance.company = self.request.user.company
        return super(InvitationCreateView, self).form_valid(form)
